# from django.shortcuts import render, get_object_or_404
# from .models import Album, Song
#
#
# def index(request):
#     all_albums = Album.objects.all()
#     return render(request, 'music/index.html', {"all_albums": all_albums})
#
#
# def detail(request, album_id):
#
#     # Detailed way to get an object or throw 404 error is object not present
#     # Writing here for revision
#
#     # try:
#     #     album = Album.objects.get(pk=album_id)
#     # except Album.DoesNotExist:
#     #     raise Http404("Album does not exist")
#
#     # This block of code does exactly what is done above by try except statement
#     album = get_object_or_404(Album, pk=album_id)
#     return render(request, 'music/detail.html', {"album": album})
#
#
# def favorite(request, album_id):
#     album = get_object_or_404(Album, pk=album_id)
#     try:
#         selected_song = album.song_set.get(pk=request.POST['song'])
#     except(KeyError, Song.DoesNotExist):
#         return render(request, 'music/detail.html', {
#             "album": album,
#             "error_message": "You have not selected a valid song "
#         })
#     else:
#         selected_song.is_favourite = True
#         selected_song.save()
#         return render(request, 'music/detail.html', {"album": album})



###############################################################################################################################################################
###############################################################################################################################################################

# This is the new way we are going to create views using generic view pattern

from django.views import generic
from .models import Album, Song


class IndexView(generic.ListView):
    template_name = 'music/index.html'
    context_object_name = 'all_albums'

    def get_queryset(self):
        return Album.objects.all()


class DetailView(generic.DetailView):
    model = Album
    template_name = 'music/detail.html'

